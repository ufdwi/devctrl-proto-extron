import { ExtronDXPCommunicator } from "./Extron/ExtronDXPCommunicator";
import {ExtronSWUSBCommunicator} from "./Extron/ExtronSWUSBCommunicator";

//devctrl-proto-package
module.exports = {
    communicators: {
        'Extron/DXP' : ExtronDXPCommunicator,
        'Extron/SWUSB' : ExtronSWUSBCommunicator
    }
};